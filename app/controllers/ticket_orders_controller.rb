class TicketOrdersController < ApplicationController
  before_filter :require_login
  before_filter :find_ticket_order, :only =>[:edit, :update, :destroy, :check_out]

  def index
    if current_user && !current_user.is_admin?
      @ticket_orders = TicketOrder.where("user_id = #{current_user.id}")
    elsif 
      @ticket_orders = TicketOrder.all
    end
  end

  def show
    @ticket_order = TicketOrder.find(params[:id])
  end

  def new
    tickets =  Ticket.where("is_ordered is false")
    @tickets = tickets.map{|x| [x.code,x.id]}  if tickets
    @ticket_order = TicketOrder.new
    if params[:ticket_id]
      @ticket_id = params[:ticket_id]
    end
  end

  def edit
    @tickets = Ticket.where("is_ordered is false").map{|x| [x.code,x.id]} << ["#{@ticket_order.ticket.code}","#{@ticket_order.ticket.id}"]
    @ticket_id = @ticket_order.ticket.id
    if @ticket_order.is_paid
      flash[:notice] = 'This Order has been paid.'
      redirect_to ticket_orders_path
    end
  end

  def create
    @ticket_order = TicketOrder.new(params[:ticket_order].merge(:order_date => Time.now, :is_paid => false))
    if @ticket_order.save
      Ticket.find(params[:ticket_order][:ticket_id]).update_attribute(:is_ordered, true)
      redirect_to ticket_orders_path, :notice=> 'Ticket order was successfully created.'
    else
      render :action =>"new"
    end
  end
  
  def update    
    if @ticket_order.update_attributes(params[:ticket_order])
      redirect_to ticket_orders_path, :notice=> 'Ticket order was successfully updated.'
    else
      render :action=> "edit"
    end
  end

  def destroy    
    @ticket_order.destroy
    redirect_to ticket_orders_path, :notice=> 'Ticket order was successfully deleted.'
  end

  def check_out   
    unless current_user.card_number.nil? or current_user.card_number == ""
      @ticket_order.update_attribute(:is_paid , true)
      flash[:notice] = 'Ticket order was successfully paid.'
    else
      flash[:notice] = 'Please complete your credit card number first.'
      redirect_to edit_user_path(current_user)
    end
  end

  private
  def find_ticket_order
    @ticket_order = TicketOrder.find(params[:id])
  end
end
