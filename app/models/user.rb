class User < ActiveRecord::Base
  attr_accessible :email, :password, :password_confirmation,:name, :address,:card_number
  attr_accessor :password
  before_save :encrypt_password
  has_many :ticket_orders
  has_many :tickets
  validates :password, :presence => {:on => :create},
    :confirmation => true
  validates :email, :presence => true, :uniqueness => true

  def is_admin?
    if self.email == 'admin@admin.com'
      true
    else
      false
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end
