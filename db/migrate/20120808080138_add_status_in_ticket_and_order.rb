class AddStatusInTicketAndOrder < ActiveRecord::Migration
  def up
    add_column :ticket_orders ,:is_paid , :boolean
    add_column :tickets ,:is_ordered, :boolean
  end

  def down
    remove_column :ticket_orders ,:is_paid 
    remove_column :tickets ,:is_ordered
  end
end
