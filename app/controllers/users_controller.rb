class UsersController < ApplicationController
  before_filter :find_user, :only => [:update, :destroy, :edit, :show]
  def index
    @users = User.all
  end

  def show;end

  def new
    @user = User.new
  end

  def edit;end

  def create
    @user = User.new(params[:user])
    if verify_recaptcha
      if @user.save        
        redirect_to root_url, :notice => "User was successfully created. Please Login first"
      else
        render "new" 
      end
    else
      flash[:error] = "There was an error with the recaptcha code below.
                     Please re-enter the code and click submit."
      render "new"
    end
  end

  def update   
    if @user.update_attributes(params[:user])
      redirect_to root_path
      flash[:notice] = 'User was successfully updated.'
    else
      render :action => "edit"
    end
  end

  def destroy    
    @user.destroy
  end

  private
  def find_user
    @user = User.find(params[:id])
  end
end
