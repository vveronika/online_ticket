class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.datetime :departure
      t.string :code
      t.string :destination
      t.string :ticket_type
      t.string :ticket_class
      t.integer :user_id
      t.timestamps
    end
  end
end
