class Ticket < ActiveRecord::Base
  attr_accessible :code, :departure, :destination, :ticket_type, :ticket_class, :user_id, :is_ordered
  has_one :ticket_order
  validates :code,  :presence => true , :uniqueness => true
  validates :departure,  :presence => true
  validates :destination,  :presence => true
  validates :ticket_type,  :presence => true
  validates :ticket_class,  :presence => true
  validate :check_departure

  def check_departure
    errors.add(:departure, "error : Please complete your departure date and time") if departure.nil?
  end

end
