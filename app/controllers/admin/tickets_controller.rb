class Admin::TicketsController < Admin::ApplicationController
  before_filter :find_ticket,  :only => [:edit, :update, :destroy, :show]
  before_filter :require_admin_login
  def index
    @tickets = Ticket.all
  end

  def show;
  end

  def new
    @ticket = Ticket.new
  end

  def edit;
  end

  def create
    @ticket = Ticket.new(params[:ticket].merge(:is_ordered => false))
    if @ticket.save
      redirect_to admin_tickets_path, :notice => 'Ticket was successfully created.'
    else
      render :action => "new", :notice => 'Ticket was failed created.'
    end
  end

  def update
    if @ticket.update_attributes(params[:ticket])
      redirect_to admin_tickets_path, :notice => 'Ticket was successfully updated.'
    else
      render :action =>"edit", :notice => 'Ticket was failed updated.'
    end
    
  end

  def destroy
    @ticket.destroy
    redirect_to admin_tickets_path, :notice =>"Ticket was successfully deleted"
  end

  private
  def find_ticket
    @ticket = Ticket.find(params[:id])
  end
end
