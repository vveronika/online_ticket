class DashboardController < ApplicationController
  def index
    @tickets = Ticket.where("is_ordered = false")
  end
end
