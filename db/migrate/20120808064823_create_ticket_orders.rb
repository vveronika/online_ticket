class CreateTicketOrders < ActiveRecord::Migration
  def change
    create_table :ticket_orders do |t|
      t.integer :user_id
      t.integer :ticket_id
      t.string :order_number
      t.date :order_date
      t.string :sit_number
      t.timestamps
    end
  end
end
