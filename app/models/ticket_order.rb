class TicketOrder < ActiveRecord::Base
  attr_accessible :order_number, :order_date, :sit_number, :ticket_id, :user_id, :is_paid
  belongs_to :user
  belongs_to :ticket
  validates :sit_number , :presence => true,  :uniqueness => {:message => " has ordered"}
  validates :ticket_id  , :presence => true
  validates :order_number  , :presence => true,  :uniqueness => true
  
end
