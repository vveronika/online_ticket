class TicketsController < ApplicationController
  before_filter :find_ticket,  :only => [:show]
 
  def index
    @tickets = Ticket.where("is_ordered = false")
  end

  def show;  end

  private
  def find_ticket
    @ticket = Ticket.find(params[:id])
  end
end
